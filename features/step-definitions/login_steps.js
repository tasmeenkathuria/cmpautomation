const { Given, When, Then } = require('@cucumber/cucumber');
const { expect } = require('chai')
const LoginPage = require('../pageobjects/login.page');
const HomePage = require('../pageobjects/home.page');

const pages = {
    login: LoginPage,
}

Given(/^I am on the (\w+) page$/, async (page) => {
    await pages[page].open()
});

When(/^I login with admin user$/, async function () {
    await LoginPage.login("admin@admin.com", "123456")
});

Then(/^I should be on taxonomy page$/, async () => {
    await HomePage.waitForUrl()
    let url = await browser.getUrl();
    expect(url).to.include('taxonomy')

});

Then(/^I should be on "(.*)" page$/, async (pageName) => {
    await HomePage.waitForUrl()
    let url = await browser.getUrl();
    expect(url).to.include(pageName)

});

