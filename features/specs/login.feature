Feature: Test the CMP log in page

  
  Background:
    Given I am on the login page
  Scenario Outline: As a user, I can log into CMP
    When I login with admin user
    Then I should be on "taxonomy" page