const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */

class LoginPage extends Page {
    
    /**
     * define selectors using getter methods
     */
    get inputUsername () { return $('#mainLoginEmail') } 
    get inputPassword () { return $('#mainLoginPassword') }
    get btnSignIn () { return $('//*[text()="Sign In"]') }

    /**
     * a method to encapsule automation code to interact with the page
     * e.g. to login using username and password
     */
    async login (username, password) {
        await this.inputUsername.setValue(username);
        await this.inputPassword.setValue(password);
        await this.btnSignIn.click();
    }

    /**
     * overwrite specifc options to adapt it to page object
     */
    async open () {
        return super.open('login');
    }
}

module.exports = new LoginPage();
