/**
* main page object containing all methods, selectors and functionality
* that is shared across all page objects
*/
module.exports = class Page {
    /**
    * Opens a sub page of the page
    * @param path path of the sub page (e.g. /path/to/page.html)
    */
  
    async open (path) {
        //var baseURL = this.getBaseUrl()
        await browser.url(`/${path}`)
    }

    /*getBaseUrl (){
        return "https://chp-cmp-dot-test-ch-central.uc.r.appspot.com/"
    }*/

    async waitForUrlContaining(text){
        await browser.waitUntil(async () => {
            let pageUrl = await browser.getUrl();
            return pageUrl.indexOf(text) > -1
              
          }, 10000,'Could not load the page')
    }
    
}
