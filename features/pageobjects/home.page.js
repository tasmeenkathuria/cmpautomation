const Page = require('./page');

/**
 * sub page containing specific selectors and methods for a specific page
 */


 class HomePage extends Page {
     /**
     * define selectors using getter methods
     */
     
     /**
     * a method to encapsule automation code to interact with the page
     */
    async waitForUrl() {
        await super.waitForUrlContaining('taxonomy')

    }
 }
 module.exports = new HomePage();